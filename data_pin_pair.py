#!/usr/bin/env python
# -*- coding: utf-8 -*-

from uv_prog_pin import GPIO

# line : (in_bbb, out_bbb)
uvprogDataPins = {
    "D0" : (113, 51),
    "D1" : (49, 22),
    "D2" : (38, 60),
    "D3" : (39, 30),
    "D4" : (15, 31),
    "D5" : (14, 48),
    "D6" : (111, 66),
    "D7" : (112, 68)
}

# THEN INSTALL circuit in socket select !!!!WRITE MODE!!!!

class uvprogDataPinPair:
    def __init__(self, line):
        valid_lines = uvprogDataPins.keys()
        if not line in valid_lines:
            raise ValueError('{} is invalid line name.'.format(line))
        self.line = line

        self.inpin = GPIO(uvprogDataPins[self.line][0])
        self.inpin.set_dir('in') # safe
        self.inpin.inverted = True
        self.outpin = GPIO(uvprogDataPins[self.line][1])
        self.outpin.set_dir('out') # unsafe in read mode KEEP CLAIM
        self.outpin.set_val(0)

    def set_mode(self, mode):
        if mode == 'r':
            # if read mode dala lines controls by 5 v logical 1,
            # so write logical 0 to the pin
            self.outpin.set_val(0)
        elif mode == 'w':
            pass
        else:
            raise ValueError('Only r/w mode accepted')

    def write(self, value):
        self.outpin.set_val(value)

    def read(self):
        self.set_mode('r')
        return self.inpin.get_val()

    def _raw_read(self):
        return self.inpin.get_val()

    def test(self, value):
        self.outpin.set_val(value)
        _in = self.inpin.get_val()
        return _in

# with no circut in socket!
def test():
    for pinname in uvprogDataPins.keys():
        print('--- Testing pin {}'.format(pinname))
        pin = uvprogDataPinPair(pinname)
        if pin.test(0) == 0 and pin.test(1) == 1:
            print('test: PASSED')
        else:
            print('test: FAILED')
            while True:
                print('--- Manual test ---')
                d = input('Test set value (1/0): ')
                try:
                    print ('in={}'.format(pin.test(d)))
                except:
                    print('Exit.')
                    return

if __name__ == '__main__':
    test()
